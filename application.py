# Autor: Christopher Mahn
from playsound import playsound


file = open("database.csv", "r")
database = file.readlines()
file.close()

for index, item in enumerate(database):
    database[index] = item.split(",")
    database[index][1] = int(database[index][1])

scanned_list = []

print("to exit: type 'exit'")
while(True):
    scanned_item = input()
    if(scanned_item == "exit"):
        break
    scanned_list.append(scanned_item)
    audio = False
    for i in database:
        if(scanned_item == i[0]):
            if(i[1] > 12):
                playsound(f"dont_keep_item.mp3")
                audio = True
                break
    if audio is False:
        playsound(f"keep_item.mp3")

# sorting
scanned_list.sort()

# removing duplicates from list
scanned_list = list(dict.fromkeys(scanned_list))

for i in range(len(scanned_list)):
    duration = 1
    for j in database:
        if(scanned_list[i] == j[0]):
            duration = j[1]+1
    scanned_list[i] = f"{scanned_list[i]}, {duration}"


file = open("database.csv", "w")
for i in scanned_list:
    file.write(f"{i}\n")
file.close()
