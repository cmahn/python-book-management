# python-book-management

A small little python-skript for managing how long items have been stored, in order to sort out items with little or no demand.

More information at: [www.cmahn.de](https://www.cmahn.de/2020/02/19/einfache-buchverwaltung-mit-barcode-scanner-und-python/)