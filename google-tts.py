
from playsound import playsound
from gtts import gTTS


def speak(string):
    tts = gTTS(string)
    tts.save('gTTS.mp3')
    playsound("gTTS.mp3")
